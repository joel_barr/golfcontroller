﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAndShoot : MonoBehaviour {

    public Vector2 aimSpeed;
    public float power = 0.5f;
    public float shotpower = 10f;

    public GameObject aimer;
    public GolfBall ball;

	// Use this for initialization
	void Start () {
        ball = GetComponentInParent<GolfBall>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        transform.Rotate(0f, aimSpeed.x * Input.GetAxis("Horizontal"), 0f, Space.World);
        float vAngle = aimSpeed.y * Input.GetAxis("Vertical");
        aimer.transform.RotateAround(transform.position, transform.forward, vAngle);
        if (Input.GetKeyDown(KeyCode.E)) power += 0.25f;
        else if (Input.GetKeyDown(KeyCode.Q)) power -= 0.25f;

        aimer.transform.localScale = new Vector3(aimer.transform.localScale.x, power, aimer.transform.localScale.z);

        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        GetComponentInChildren<Rigidbody>().AddForce(aimer.transform.up * power * shotpower);
    }
}
