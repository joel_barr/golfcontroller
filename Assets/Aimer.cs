﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aimer : MonoBehaviour {

    //public Transform _aimDisplayObject;
    public Vector2 aimSpeed;
    float power = 0f;
    public float powerSpeed;
    
    public event System.Action<Aimer, Vector3> OnShotSet;
    public event System.Action<float> OnPowerChanged;

    public Vector3 AimVector
    {
        get { return transform.forward;  }
        set { transform.forward = value; }
    }


    // Update is called once per frame
    void Update ()
    {
        // Process shoot button
        if (Input.GetButtonDown("Fire1")) power = 0f;
        else if (Input.GetButton("Fire1"))
        {
            power += powerSpeed * Time.deltaTime;
            if (OnPowerChanged != null) OnPowerChanged(power);
        }

        //Shoot
        if (Input.GetButtonUp("Fire1") || power >= 1.0f)
        {
            Vector3 eulers = transform.eulerAngles;
            eulers.x = -FindObjectOfType<ArcRenderer>().Angle;
            transform.eulerAngles = eulers;
            Vector3 poweredvector = transform.forward * power;
         
            if (OnShotSet != null) OnShotSet(this, poweredvector);
        }

    }
}
