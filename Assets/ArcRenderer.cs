﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcRenderer : MonoBehaviour {

    LineRenderer lr;

    public float velocity;
    public float pitchSpeed = 1.5f;
    public int segments = 5;

    float g;
    float minPower, maxPower;
    public Transform beacon;
    public float radAngle = 0f;

    public float Angle
    {
        get { return Mathf.Rad2Deg * radAngle; }
        //set { radAngle = value * Mathf.Deg2Rad; }
    }

	// Use this for initialization
	void Awake () {
        lr = GetComponent<LineRenderer>();
        g = Mathf.Abs(Physics.gravity.y);
	}

    private void Start()
    {
        RenderArc();
        GolfBall ball = GetComponentInParent<GolfBall>();

        if (ball != null)
        {
            minPower = ball.MinPower;
            velocity = maxPower = ball.MaxPower;

            Aimer aim = ball.Aimer;
            if (aim != null)
            {
                aim.OnPowerChanged += OnPowerChanged;
            }
        }

        RenderArc();
        
    }

    void Update()
    {
        if (Input.GetButton("Vertical"))
        {
            float adjustment = pitchSpeed * Input.GetAxis("Vertical");
            
            radAngle += adjustment * Mathf.Deg2Rad;

            RenderArc();
        }
    }

    private void OnPowerChanged(float obj)
    {
        velocity = minPower + (Mathf.Clamp01(obj) * (maxPower - minPower));
        RenderArc();
    }

    private void RenderArc()
    {
        lr.positionCount = segments + 1;
        lr.SetPositions(CalculateArcArray());
    }

    private Vector3[] CalculateArcArray()
    {
        Vector3[] arc = new Vector3[segments + 1];
        float maxDistance = (velocity * velocity * Mathf.Sin(2 * radAngle)) / g;

        for (int i = 0; i <= segments; i++)
        {
            float t = (float)i / (float)segments;
            float x = t * maxDistance;
            float y = (x * Mathf.Tan(radAngle)) - ((g * x * x) / (2 * Mathf.Pow(velocity * Mathf.Cos(radAngle), 2)));
            arc[i] = new Vector3(0f, y, x);
        }

        beacon.localPosition = arc[arc.Length - 1];
        return arc;
    }


}
