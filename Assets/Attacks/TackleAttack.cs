﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TackleAttack : Attack
{
    public float travelTime = 0.5f, kick = 10, upKick = 1, distance = 2;
    public float closenessThreshold = 0.01f;
    public GameObject aimer;

    float Speed { get { return distance / travelTime; } }
    
    public override void Equip(GolfBall golfBall)
    {
        base.Equip(golfBall);
        InputManager.OnShootUp += Charge;
        InputManager.OnHorizontalHeld += ball.Turn;
    }

    private void Charge()
    {
        Vector3 destination = transform.position + (transform.forward * distance);
        StartCoroutine(Travel(destination));

        InputManager.OnShootUp -= Charge;
        InputManager.OnHorizontalHeld -= ball.Turn;
        
    }

    private void OnDestroy()
    {
        InputManager.OnShootUp -= Charge;
        InputManager.OnHorizontalHeld -= ball.Turn;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Vector3 direction = new Vector3() ;

            Vector3 perp = Vector3.Cross(transform.forward, other.transform.position - this.transform.position);
            float dir = Vector3.Dot(perp, Vector3.up);

            if (dir > 0f)
            {
                direction = (transform.right + transform.forward) / 2f;
            }
            else
            {
                direction = (-transform.right + transform.forward) / 2f;
            }

            Vector3 force = direction.normalized * kick;
            force.y = upKick;
            other.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);
        }
        else if (other.GetComponent<Terrain>() != null)
        {
            StopCoroutine("Travel");
            EndTravel();
        }
    }

    IEnumerator Travel(Vector3 destination)
    {
        GetComponentInChildren<Collider>().enabled = true;
        Destroy(aimer);
        ball.RigidBody.isKinematic = true;
        ball.GetComponent<Collider>().enabled = false;

        while (Vector3.Distance(transform.position, destination) > closenessThreshold)
        {
            Debug.DrawLine(transform.position, destination);
            ball.transform.Translate(transform.forward * Speed * Time.deltaTime, Space.World);
            yield return null;
        }

        EndTravel();
    }

    private void EndTravel()
    {
        Destroy(GetComponent<Collider>());
        Destroy(GetComponentInChildren<ParticleSystem>());
        ball.GetComponent<Collider>().enabled = true;
        ball.RigidBody.isKinematic = false;
        
        ball.EndAttackAnimation();
    }
}
