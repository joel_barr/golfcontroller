﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack : MonoBehaviour {

    protected GolfBall ball;

    public virtual void Equip(GolfBall golfBall)
    {
        ball = golfBall;
    }
}
