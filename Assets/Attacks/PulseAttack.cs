﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseAttack : Attack
{
    public float radius = 3f, force = 20f, upMod = 1;
    public float burstTime = 0.2f, burstSize = 6f;

    public override void Equip(GolfBall golfBall)
    {
        base.Equip(golfBall);
        InputManager.OnShootUp += Pop;
        transform.localScale = Vector3.one * 2 * radius;
    }

    private void Pop()
    {
        Vector3 epicentre = transform.position;
        Collider[] colliders = Physics.OverlapSphere(epicentre, radius);
        foreach (Collider c in colliders)
        {
            if (c.GetComponent<Rigidbody>() == null || c.gameObject == transform.parent.gameObject) continue;

            c.GetComponent<Rigidbody>().AddExplosionForce(force, epicentre, radius, upMod, ForceMode.Impulse);
        }

        InputManager.OnShootUp -= Pop;
        StartCoroutine(ShowBurst());
    }

    IEnumerator ShowBurst()
    {
        float timeSpent = 0f;
        float scalePerSecond = (burstSize - radius) / burstTime;
        while (timeSpent < burstTime)
        {
            // Expand sphere
            radius += scalePerSecond * Time.deltaTime;
            transform.localScale = Vector3.one * 2 * radius;
            
            //Tick
            timeSpent += Time.deltaTime;
            yield return null;
        }

        Finish();
    }

    private void Finish()
    {
       ball.EndAttackAnimation();
    }

    private void OnDestroy()
    {
        InputManager.OnShootUp -= Pop;
    }
}
