﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GolfBall : MonoBehaviour {
    public enum BallState { Sleeping, BowlAiming, Bowling, AttackAiming, Attacking }

    public float sleepVelocity = 0.01f; //The speed at which the ball will stop moving
    [SerializeField] float minPower = 100f, maxPower = 1000f;
    Aimer aimer;
    Rigidbody _rgdbdy;
    public Aimer aimerPrefab;
    public float turnSpeed = 1.5f;
    public List<Attack> attacks;

    BallState state = BallState.Sleeping;
    public ForceMode forceMode;

    Action onTurnEnd;
    private Attack equippedAttack;
    private int selectedAttk = 0;

    public float MinPower { get { return minPower; } }

    internal void BeginTurn(Action onTurnEnd)
    {
        this.onTurnEnd = onTurnEnd;
        InputManager.OnHorizontalHeld += Turn;
        BeginAim();
    }

    internal void EndAttackAnimation()
    {
        if (state != BallState.AttackAiming) throw new AccessViolationException("Cannot Attack when not Aiming");

        NextState();
    }

    public float MaxPower { get { return maxPower; } }

    public Aimer Aimer { get { return aimer; } }

    public Rigidbody RigidBody
    {
        get
        {
            if (_rgdbdy == null)
            {
                _rgdbdy = GetComponent<Rigidbody>();
            }

            return _rgdbdy;
        }
    }

    public void Turn(float value)
    {
        transform.Rotate(0f, value * turnSpeed, 0f, Space.World);
    }

    // Update is called once per frame
    void Update () {
        if (state == BallState.Bowling && RigidBody.velocity.magnitude < sleepVelocity)
        {
            NextState();
        }
	}

    public void Shoot(Aimer sender, Vector3 shotVector)
    {
        if (state != BallState.BowlAiming)
        {
            Debug.LogError("Cannot shoot a ball that is not aiming!");
            return;
        }
        //UnityEditor.EditorApplication.isPaused = true;
        Destroy(sender.gameObject);

        float power = MinPower + (Mathf.Clamp01(shotVector.magnitude) * (maxPower - MinPower));
        Vector3 poweredVector = shotVector.normalized * power;
        RigidBody.AddForce(poweredVector, forceMode);

        NextState();
    }

    void NextState()
    {
        if (state == BallState.Sleeping)
        {
            RigidBody.velocity = Vector3.zero;
            RigidBody.angularVelocity = Vector3.zero;
            Reorient();
            aimer = Instantiate(aimerPrefab, transform.position, Quaternion.identity, this.transform);
            aimer.OnShotSet += Shoot;

            state = BallState.BowlAiming;
        }
        else if (state == BallState.BowlAiming)
        {
            InputManager.OnHorizontalHeld -= Turn;
            state = BallState.Bowling;
        }
        else if (state == BallState.Bowling)
        {
            Halt();

            //Add attack behaviour
            Equip(attacks[0]);
            InputManager.OnAtkNavDown += NavigateAttacks;

            state = BallState.AttackAiming;
        }
        else if (state == BallState.AttackAiming) //Attack launched
        {
            InputManager.OnAtkNavDown -= NavigateAttacks;

            state = BallState.Attacking;
            FindObjectOfType<PlayerManager>().OnStillnessAchieved = NextState;
        }
        else if (state == BallState.Attacking)
        {
            //Remove attack behaviour
            Unequip();

            onTurnEnd();
            state = BallState.Sleeping;
        }
    }

    public void Halt()
    {
        //Stop moving
        RigidBody.velocity = Vector3.zero;
        Reorient();
    }

    private void NavigateAttacks(int direction)
    {
        //Unequip old attack
        Unequip();
        //Increment selected attack
        if (direction < 1 && selectedAttk == 0)
            selectedAttk = attacks.Count - 1;
        else
            selectedAttk = (selectedAttk + direction) % attacks.Count;

        //Equip new attack
        Equip(attacks[selectedAttk]);
    }

    private void Reorient()
    {
        transform.rotation = Quaternion.identity;
    }

    private void Unequip()
    {
        if (equippedAttack != null)
            Destroy(equippedAttack.gameObject);
    }


    private void Equip(Attack attackTemplate)
    {
        equippedAttack = Instantiate<Attack>(attackTemplate, this.transform, false);

        equippedAttack.Equip(this);
    }

    void BeginAim()
    {
        if (state != BallState.Sleeping)
            throw new AccessViolationException("Cannot begin aiming while not sleeping");

        NextState();
    }
}
