﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pylon : MonoBehaviour {

    public Material whiteMat, blueMat, greenMat, redMat, yelloMat;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        GolfBall ball = other.GetComponent<GolfBall>();
        if (ball != null)
        {
            string ballMatName = ball.GetComponentInChildren<MeshRenderer>().material.ToString();
            switch (ballMatName)
            {
                case "SolidBlue (Instance) (UnityEngine.Material)":
                    this.GetComponentInChildren<MeshRenderer>().material = blueMat;
                    break;
                case "SolidGreen (Instance) (UnityEngine.Material)":
                    this.GetComponentInChildren<MeshRenderer>().material = greenMat;
                    break;
                case "SolidRed (Instance) (UnityEngine.Material)":
                    this.GetComponentInChildren<MeshRenderer>().material = redMat;
                    break;
                case "SolidYellow (Instance) (UnityEngine.Material)":
                    this.GetComponentInChildren<MeshRenderer>().material = yelloMat;
                    break;
            }
            
        }
    }
}
