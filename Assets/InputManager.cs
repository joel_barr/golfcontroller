﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    //Shoot Button
    public const string SHOOT_BTN_NAME = "Fire1";
    public static event Action OnShootDown;
    public static event Action OnShootHeld;
    public static event Action OnShootUp;

    //Horizontal Axis
    public static event Action<float> OnHorizontalStart;
    public static event Action<float> OnHorizontalHeld;
    public static event Action<float> OnHorizontalStop;

    //Attack Nav Buttons
    public const string ATTACK_NAV_BTN_NAME = "NavAttack";
    public static event Action<int> OnAtkNavDown;
    //public static event Action<int> OnAtkNavHeld;
    //public static event Action<int> OnAtkNavUp;

	
    // Update is called once per frame
	void Update () {
        // Shoot
        if (Input.GetButtonDown(SHOOT_BTN_NAME) && OnShootDown != null) OnShootDown();
        else if (Input.GetButtonUp(SHOOT_BTN_NAME) && OnShootUp != null) OnShootUp();
        else if (Input.GetButton(SHOOT_BTN_NAME) && OnShootHeld != null) OnShootHeld();

        // Horizontal Axis
        if (Input.GetButtonDown("Horizontal") && OnHorizontalStart != null) OnHorizontalStart(Input.GetAxis("Horizontal"));
        else if (Input.GetButtonUp("Horizontal") && OnHorizontalStop != null) OnHorizontalStop(Input.GetAxis("Horizontal"));
        else if (Input.GetButton("Horizontal") && OnHorizontalHeld != null) OnHorizontalHeld(Input.GetAxis("Horizontal"));

        //Navigate attacks
        if (Input.GetButtonDown(ATTACK_NAV_BTN_NAME) && OnAtkNavDown != null) OnAtkNavDown(Input.GetAxis(ATTACK_NAV_BTN_NAME) >= 0 ? 1 : -1);
        //else if (Input.GetButtonUp(ATTACK_NAV_BTN_NAME) && OnAtkNavUp != null) OnAtkNavUp(Input.GetAxis(ATTACK_NAV_BTN_NAME) >= 0 ? 1 : -1);
        //else if (Input.GetButton(ATTACK_NAV_BTN_NAME) && OnAtkNavHeld != null) OnAtkNavHeld(Input.GetAxis(ATTACK_NAV_BTN_NAME) >= 0 ? 1 : -1);
    }
}
