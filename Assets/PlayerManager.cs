﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class PlayerManager : MonoBehaviour
{

    public GolfBall[] balls;
    public int currentPlayer = 0;
    private Action _onStillnessAchieved;

    // Use this for initialization
    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        BeginTurn();
    }

    private void BeginTurn()
    {
        GolfBall curBall = balls[currentPlayer];

        //Move camera to current player
        FreeLookCam cam = FindObjectOfType<FreeLookCam>();
        cam.SetTarget(curBall.transform);

        curBall.BeginTurn(OnTurnEnd);
    }

    /// <summary>
    /// Gets the action to be performed when all balls are still, or sets it and begins waiting
    /// </summary>
    /// <value>
    /// The Action to be performed when all balls are still
    /// </value>
    public Action OnStillnessAchieved
    {
        get
        {
            return _onStillnessAchieved;
        }
        internal set
        {
            StartCoroutine(PursueStillness());
            _onStillnessAchieved = value;
        }
    }

    /// <summary>
    /// Pursues the stillness.
    /// </summary>
    /// <returns></returns>
    private IEnumerator PursueStillness()
    {
        bool stillnessAchieved;
        yield return new WaitForEndOfFrame();
        do
        {
            stillnessAchieved = true; //Assume true
            foreach (GolfBall ball in balls)
            {
                Rigidbody rb = ball.GetComponent<Rigidbody>();
                if (rb != null && rb.velocity.magnitude < ball.sleepVelocity)
                {
                    ball.Halt(); //Make sure that the ball is completely stopped
                }
                else
                {
                    stillnessAchieved = false; //False if any ball is in motion
                }
            }
            yield return new WaitForSeconds(0.5f); //Wait half a second
        } while (!stillnessAchieved);

        OnStillnessAchieved();
    }

    private void NextPlayer()
    {
        currentPlayer = (currentPlayer + 1) % balls.Length;
        BeginTurn();
    }

    private void OnTurnEnd()
    {
        NextPlayer();
    }
}
